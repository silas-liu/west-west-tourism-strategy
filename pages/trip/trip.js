const api = require('../../utils/api')
const app = getApp()

Page({
  data:{
    selectedTrip:{},
    trips:[
      {
        id:1,
        name:'大漠孤烟直，长河落日圆',
        first_day:'2020.09.08',
        likes:220003,
        view_count:2223786,
        days: [
          {
            day:'一',
            imgSrc:'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2336774491,806537954&fm=26&gp=0.jpg',
            desc:'甘肃省4个国家历史文化名城:敦煌,武威,张掖,天水',
            data:'2020.08.07'
          },
          {
            day:'二',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605069786474&di=b7171192e48b74d7454ba6a536c93e0c&imgtype=0&src=http%3A%2F%2Fdingyue.ws.126.net%2F2019%2F1212%2F8e8ed742j00q2dir7004mc200u000ixg00gb00aa.jpg',
            desc:'龙门石窟',
            data:'2020.08.08'
          },
          {
            day:'三',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605069786474&di=8172e6b67426073fd3354067ba463fc9&imgtype=0&src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fimages%2F20180225%2Febbe626874ab4a86bc9ae5c7f79497bc.jpeg',
            desc:'沙漠风光',
            data:'2020.08.09'
          },
          {
            day:'四',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605069786472&di=b6eae2c3474ebd6ddb64286698275614&imgtype=0&src=http%3A%2F%2Fzkres1.myzaker.com%2F201901%2F5c3701a2622768b8ab000000_1024.jpg',
            desc:'敦煌博物馆',
            data:'2020.08.10'
          }
        ]
      },
      {
        id:2,
        name:'自然风景有感情的形容词',
        first_day:'2020.10.12',
        likes:280503,
        view_count:2234786,
        days: [
          {
            day:'一',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605070295175&di=269a855189331a0a03f2601970874ddc&imgtype=0&src=http%3A%2F%2Fimg.mp.sohu.com%2Fupload%2F20170815%2Fa0b4fd6a9e384a54bbf866743378778e_th.png',
            desc:'被《消失的地平线》极力推荐的香格里拉,值得你去',
            data:'2020.10.12'
          },
          {
            
            day:'二',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605070214376&di=0cdb13ed198d054f5cc75a544bfdf7e1&imgtype=0&src=http%3A%2F%2Fimg8.zol.com.cn%2Fbbs%2Fupload%2F12688%2F12687055.jpg',
            desc:'普达措国家森林公园(香格里拉)',
            data:'2020.10.13'
          },
     
          {
            day:'三',
            imgSrc:'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=818938591,2422186581&fm=26&gp=0.jpg',
            desc:'雪山之畔，朝圣之路',
            data:'2020.10.14'
          },
          {
            day:'四',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605070295174&di=5daf0b10b7e2af0d079f033b4cbf3c08&imgtype=0&src=http%3A%2F%2Fimg1.doubanio.com%2Fview%2Fnote%2Fl%2Fpublic%2Fp50784297.jpg',
            desc:'美丽而又宁静的香格里拉,无论是悠闲自在的蓝天白云,还是壮...',
            data:'2020.10.15'
          }
        ]
      },
      {
        id:3,
        name:'山川画卷',
        first_day:'2020.7.20',
        likes:330503,
        view_count:4434786,
        days: [
          {
            day:'一',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605070987968&di=8b1db3dd66169cae4c783c4f8fa85c24&imgtype=0&src=http%3A%2F%2Fhbimg.b0.upaiyun.com%2F60a76d4893f1d241c855191fdd53c60ab3d2badaedad-dnFryb_fw658',
            desc:'从你的全世界路过~',
            data:'2020.7.20'
          },
          {
            
            day:'二',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605070987968&di=8187a1114b220d3a69fda598afb87257&imgtype=0&src=http%3A%2F%2Fimg.ivsky.com%2Fimg%2Ftupian%2Fpre%2F201512%2F05%2Fscenic_spot_of_daocheng_yading-013.jpg',
            desc:'宁静以致远~',
            data:'2020.7.21'
          },
     
          {
            day:'三',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605070987967&di=65e06a65604a9e925ca91257f43bf50d&imgtype=0&src=http%3A%2F%2Fimg.ivsky.com%2Fimg%2Ftupian%2Fpre%2F201512%2F05%2Fscenic_spot_of_daocheng_yading-004.jpg',
            desc:'看细水长流~',
            data:'2020.7.22'
          }
        ]
      },
      {
        id:4,
        name:'天空梦境',
        first_day:'2020.5.20',
        likes:5530503,
        view_count:6634786,
        days: [
          {
            day:'一',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605071369505&di=463266321f6e53c5b8fc3d0f7f20f66c&imgtype=0&src=http%3A%2F%2Fzkres2.myzaker.com%2F201708%2F59a3908ba07aec8e29014faf_640.jpg',
            desc:'" 天空之眼 " 瞰厦门,最美的风景都在这里了!',
            data:'2020.5.20'
          },
          {
            
            day:'二',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605071369507&di=2cd043259244e175ec72f8aac1a959ae&imgtype=0&src=http%3A%2F%2Fyouimg1.c-ctrip.com%2Ftarget%2Ffd%2Ftg%2Fg1%2FM01%2F9E%2F63%2FCghzfVWnHXOAOFvuABDTwctRKs0202.jpg',
            desc:'厦门-海岛风光,万国建筑,最美大学,慵懒时光',
            data:'2020.5.21'
          },
     
          {
            day:'三',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605071369507&di=f5105f87b18389ca706dc7baaf33d022&imgtype=0&src=http%3A%2F%2Fdimg02.c-ctrip.com%2Fimages%2Ftg%2F440%2F639%2F558%2F9c8675223f394b7f868a6e7c7e533a43_C_640_320.jpg',
            desc:'鼓浪屿~',
            data:'2020.5.22'
          },
          {
            day:'四',
            imgSrc:'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=3699654488,2627236760&fm=26&gp=0.jpg',
            desc:'厦门日月谷享受日月之光明',
            data:'2020.5.23'
          }
        ]
      },
      {
        id:5,
        name:'island离岛-一个人的修行',
        first_day:'2020.6.1',
        likes:330503,
        view_count:4434786,
        days: [
          {
            day:'一',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605071707054&di=96307473de6ee9bb4d5f6c7153395922&imgtype=0&src=http%3A%2F%2Fn4-q.mafengwo.net%2Fs13%2FM00%2F92%2FA5%2FwKgEaVyIcmGAS4YBAAHvAiso9gI11.jpeg',
            desc:'巴厘岛独栋泳池别墅',
            data:'2020.6.1'
          },
          {
            
            day:'二',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605071707054&di=86645908a54bc71e29efbd3a9c4a5936&imgtype=0&src=http%3A%2F%2Fimg0.selfimg.com.cn%2Fprnnews%2F2018%2F03%2F01%2Fe2d30c85b24f64c82f6f202842adebe6.jpg',
            desc:'诸神之岛:巴厘岛成为今年旅行必选地的',
            data:'2020.6.2'
          },
     
          {
            day:'三',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605071707053&di=1338a165595200d404bbfa37c3a56aba&imgtype=0&src=http%3A%2F%2Fimages5.aoyou.com%2Fproductlist%2F201706%2F6bzdnl09100407.jpg',
            desc:'孟兰岛潜水，看海底世界',
            data:'2020.7.22'
          }
        ]
      },
      {
        id:6,
        name:'漫画里的世界',
        first_day:'2020.11.08',
        likes:322503,
        view_count:3434676,
        days: [
          {
            day:'一',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605071939018&di=b53431a74f0de037454060056c7d9967&imgtype=0&src=http%3A%2F%2Fy1.ifengimg.com%2F00cd7cb92522610e%2F2012%2F1205%2Fori_50bf0a316b41a.jpeg',
            desc:'上海喜玛拉雅美术馆 《羽毛》惊天飘落',
            data:'2020.11.08'
          },
          {
            
            day:'二',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605071939018&di=809cec538a3b82d385833c327e7a504a&imgtype=0&src=http%3A%2F%2Fupload.xinhua08.com%2F2019%2F1117%2F1573994938687.png',
            desc:'上海艺术馆《重新审视超现实主义:达利与丹尼尔艺术作品展》...',
            data:'2020.11.09'
          },
     
          {
            day:'三',
            imgSrc:'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3175150902,528961630&fm=26&gp=0.jpg',
            desc:'上海美术馆大家多,其中的必点人物:刘海粟',
            data:'2020.11.10'
          },
          {
            day:'四',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605071939016&di=86741cf0ededb10bf9fc7b802a60ddf8&imgtype=0&src=http%3A%2F%2Fwww.chinartlaw.com%2Fstatics%2Fimages%2Fpost%2F20180710%2F1531209734811759.jpg',
            desc:'上海龙美术馆(西岸馆)',
            data:'2020.11.11'
          }
        ]
      },
      {
        id:7,
        name:'桂林山水甲天下',
        first_day:'2020.09.08',
        likes:220003,
        view_count:2223786,
        days: [
          {
            day:'一',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605001560827&di=d0a80d81fa9ce057aefdd907ab6f771d&imgtype=0&src=http%3A%2F%2Fi.cctcdn.com%2Fup%2Fi%2F1808%2F14%2F21aebf5d3224.jpg_600x400q85.jpg',
            desc:'纵情桂林山水，探秘民族风情',
            data:'2020.08.07'
          },
          {
            day:'二',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605001560827&di=7cf832c28e13f420164565e71f85c662&imgtype=0&src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fimages%2F20180725%2F25a7a93e6ce241e0b2c4c67632e25ff9.jpeg',
            desc:'纵情山水',
            data:'车水马龙，人世繁华，与群山绿水相依相伴~'
          },
          {
            day:'三',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605072444172&di=f2ccaffa9bce4095d812980681a62492&imgtype=0&src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fq_70%2Cc_zoom%2Cw_640%2Fimages%2F20181009%2F43fb656aa3d242c5b95f59e095e4cbfa.jpeg',
            desc:'驾轻舟穿梭在群山之中，别有风味~',
            data:'2020.08.09'
          },
          {
            day:'四',
            imgSrc:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605072858519&di=646eda592b744c6c8f56b48dae8bc4fa&imgtype=0&src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fimages%2F20180103%2F57a21d3fa10944fcb603bff057c3bcba.jpeg',
            desc:'来桂林，还有一样不容错过的，那就是桂林美食~',
            data:'2020.08.09'
          }
        ]
      }
    ],
    
    options: null,
    windowWidth: 0,
  },
  onReady() {
    const self = this;

  },
  onLoad(options){
    const id = options.id
    const name = options.name
    this.setData({
      options,
      windowWidth:app.systemInfo.windowWidth
    })
    wx.setNavigationBarTitle({
      title:options.name,
    });
    wx.showToast({
      title: '正在加载',
      icon:'loading',
      duration:1000
    })
    const trips = this.data.trips
    this.selectData(trips)
  },
  selectData:function(arr){
  
    for (const item of arr) {
      if(item.id == this.data.options.id){
        this.setData({
          selectedTrip:item
        })
      }
    }
  }
})