const App = getApp();
const api = require('../../utils/api.js');
const util = require('../../utils/util.js');

const formatTime = util.formatTime;

Page({
  data: {
    trips:[ {
      data:{
        id:1,
        name:'大漠孤烟直，长河落日圆',
        cover_image_w640:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1604988843076&di=4e88a99352f3a2e8fecb3105917cfc5a&imgtype=0&src=http%3A%2F%2Fpic1.win4000.com%2Fwallpaper%2Fb%2F50a068035604c.jpg',
        date_added:'2020.09.09',
        day_count:46,
        popular_place_str:'最美中国',
        view_count:2000,
        user:{
          name:'六月的风，八月的雨',
          avatar_1:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1604988913784&di=828730e113e13fe656436117fdadd8b1&imgtype=0&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201809%2F01%2F20180901190625_wmpeq.thumb.700_0.jpeg'
        }
      },
    type:4
  },
  {
    data:{
      id:2,
      name:'自然风景有情感的形容词',
      cover_image_w640:'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=3672347654,250980527&fm=26&gp=0.jpg',
      date_added:'2020.09.05',
      day_count:80,
      popular_place_str:'风语',
      view_count:2200,
      user:{
        name:'陌风',
        avatar_1:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1604988913783&di=bf8b65d4366c7f28586a0148af53e02f&imgtype=0&src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fitem%2F202006%2F22%2F20200622133909_rftue.thumb.400_0.jpeg'
      }
    },
  type:4
},
{
  data:{
    id:3,
    name:'山川画卷',
    cover_image_w640:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1604988842344&di=2a4667930b197888dfd860945bf46b15&imgtype=0&src=http%3A%2F%2Fimage.myyishu.com%2Fwww%2F201311%2F11%2F3970451133396992.jpg',
    date_added:'2020.10.08',
    day_count:27,
    popular_place_str:'最美中国',
    view_count:2000,
    user:{
      name:'小丸子',
      avatar_1:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1604988913782&di=54109d9f2b49637363b85cd0d7077d4d&imgtype=0&src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fitem%2F202006%2F15%2F20200615144625_WQAcx.thumb.400_0.jpeg'
    }
  },
type:4
},
{
  data:{
    id:4,
    name:'天空梦境-极致的海洋星空风景',
    cover_image_w640:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1604988842342&di=d30be4a71e6244b33c46b6e2523e6b0e&imgtype=0&src=http%3A%2F%2Fi0.hdslb.com%2Fbfs%2Farticle%2F725c03f7f1c7346291e8a4b7187e2b7ad762fd3c.jpg',
    date_added:'2020.09.01',
    day_count:23,
    popular_place_str:'天空之城',
    view_count:3000,
    user:{
      name:'二狗的梦',
      avatar_1:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1604988913780&di=d238711ec075cd25c78099e1cc3d2acd&imgtype=0&src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fitem%2F202004%2F10%2F20200410180317_ujnix.thumb.400_0.jpeg'
    }
  },
type:4
},
{
  data:{
    id:5,
    name:'islands离岛-一个人的修行',
    cover_image_w640:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1604988842342&di=751c44af89485723b6248f0a042cc2e2&imgtype=0&src=http%3A%2F%2Fimg.pconline.com.cn%2Fimages%2Fupload%2Fupc%2Ftx%2Fsoftbbs%2F1003%2F07%2Fc0%2F3134443_1267900790753_1024x1024soft.jpg',
    date_added:'2020.09.04',
    day_count:43,
    popular_place_str:'修心-修行',
    view_count:2000,
    user:{
      name:'心沐云',
      avatar_1:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1604988913779&di=a1e29f85c97c2352039ac73f725bd501&imgtype=0&src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fitem%2F202003%2F05%2F20200305105318_rvqkp.thumb.400_0.jpeg'
    }
  },
type:4
},
{
  data:{
    id:6,
    name:'漫画里的世界',
    cover_image_w640:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1604988842341&di=35327bf1c490a2691dedcba35c99f1b8&imgtype=0&src=http%3A%2F%2Fpic23.nipic.com%2F20120812%2F10388808_214429736000_2.png',
    date_added:'2020.09.12',
    day_count:36,
    popular_place_str:'最美中国',
    view_count:2000,
    user:{
      name:'安然BABY',
      avatar_1:'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=2974706256,1511610652&fm=11&gp=0.jpg'
    }
  },
type:4
},
{
  data:{
    id:7,
    name:'桂林山水甲天下',
    cover_image_w640:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1605001560829&di=d9b4c3e8de7201c7b6c911cef7fafdbb&imgtype=0&src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fimages%2F20180807%2F88775677a47b43da90bfe25148b65025.jpeg',
    date_added:'2020.09.09',
    day_count:60,
    popular_place_str:'梦中画卷',
    view_count:2000,
    user:{
      name:'心沐云',
      avatar_1:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1604988913799&di=7b162fadffd6b08625aa4faddf1b21ab&imgtype=0&src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fitem%2F202004%2F08%2F20200408222128_ZMdKw.thumb.400_0.jpeg'
    }
  },
type:4
}
],
    start: 0,
    loading: false,
    windowWidth: App.systemInfo.windowWidth,
    windowHeight: App.systemInfo.windowHeight,
  },
/*   onLoad() {
   // this.loadMore();
   this.setData({
    trips : tripsData
  })
   console.log(tripsData)

  }, */
/*   onPullDownRefresh() {
  //  this.loadMore(null, true);
  },
  loadMore(e, needRefresh) {
    const self = this;
    const loading = self.data.loading;
    const data = {
      next_start: self.data.start,
    };
    if (loading) {
      return;
    }
    self.setData({
      loading: true,
    });
    api.getHotTripList({
      data,
      success: (res) => {
        let newList = res.data.data.elements;
        console.log(newList)
        newList.map((trip) => {
          const item = trip;
          item.data[0].date_added = formatTime(new Date(item.data[0].date_added * 1000), 1);
          return item;
        });
        if (needRefresh) {
          wx.stopPullDownRefresh();
        } else {
          newList = self.data.trips.concat(newList);
        }
        self.setData({
          trips: newList,
        });
        const nextStart = res.data.data.next_start;
        self.setData({
          start: nextStart,
          loading: false,
        });
      },
    });
  }, */
  loadMore(){
    wx.showToast({
      title: '暂无更多数据',
    })
  },
  viewTrip(e) {
    const ds = e.currentTarget.dataset;
    wx.navigateTo({
      url: `../trip/trip?id=${ds.id}&name=${ds.name}`,
    });
  },
  onShareAppMessage: function () {
    return {
      title: '大白鲸旅游攻略',
      desc: '最全的旅游攻略!',
      path: 'pages/index/index?id=123'
    }
  }
});
