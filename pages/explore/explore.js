const api = require('../../utils/api')
const App = getApp()

Page({
  data:{
    elements:[
      {
        title:'长假必选景点',
        data:[
          {
            id:1,
            type:'1',
            cover_s:'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=226529392,4289109715&fm=26&gp=0.jpg',
            name:'厦门'
          },
          {
            id:2,
            type:'1',
            cover_s:'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=2381170207,3764240798&fm=26&gp=0.jpg',
            name:'成都'
          },
          {
            id:3,
            type:'1',
            cover_s:'https://ns-strategy.cdn.bcebos.com/ns-strategy/upload/fc_big_pic/part-00204-2446.jpg',
            name:'西藏'
          },
          {
            id:4,
            type:'1',
            cover_s:'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=163955504,3527866152&fm=26&gp=0.jpg',
            name:'大理'
          }
        ]
      },
      {
        title:'热门城市',
        data:[
          {
            id:5,
            type:'2',
            cover_s:'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=2742556342,3656851548&fm=26&gp=0.jpg',
            name:'厦门'
          },
          {
            id:6,
            type:'2',
            cover_s:'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3439941450,2665477004&fm=26&gp=0.jpg',
            name:'北京'
          },
          {
            id:7,
            type:'2',
            cover_s:'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3508619113,1789674571&fm=26&gp=0.jpg',
            name:'上海'
          },
          {
            id:8,
            type:'2',
            cover_s:'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=1823832464,3267100804&fm=26&gp=0.jpg',
            name:'广州'
          },
          {
            id:9,
            type:'2',
            cover_s:'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=3375085944,1870966817&fm=26&gp=0.jpg',
            name:'武汉'
          },
          {
            id:10,
            type:'2',
            cover_s:'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1037785253,2017761922&fm=26&gp=0.jpg',
            name:'三亚'
          },
          {
            id:11,
            type:'2',
            cover_s:'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=2381170207,3764240798&fm=26&gp=0.jpg',
            name:'成都'
          },
          {
            id:12,
            type:'2',
            cover_s:'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=163955504,3527866152&fm=26&gp=0.jpg',
            name:'大理'
          },
          {
            id:13,
            type:'2',
            cover_s:'https://ns-strategy.cdn.bcebos.com/ns-strategy/upload/fc_big_pic/part-00204-2446.jpg',
            name:'拉萨'
          },
          {
            id:14,
            type:'2',
            cover_s:'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2992178026,3372818827&fm=26&gp=0.jpg',
            name:'敦煌'
          }
        ]
      }
    ],
    windowWidth:App.systemInfo.windowWidth,
  },
/*   onReady(){

  },
  onload(){
    const that = this
    wx.showToast({
      title: '正在加载',
      icon:'loading',
      duration:1000
    })
    api.getExplorePlaceList({
      success:(res) => {
        const dest = res.data
        that.setData({
          elements:dest.elements
        })
        wx.hideToast({
          success: (res) => {},
        })
      }
    })
  }, */
  viewPOI(e){
    const data = e.currentTarget.dataset
    wx.navigateTo({
      url: `../destination/destination?type=${data.type}&id=${data.id}&name=${data.name}`,
    })
  },
  onShareAppMessage(){
    return {
      title:'大西西旅游攻略',
      desc:'最美旅游攻略',
      path:'pages/explore/explore?id=1'
    }
  },
  onReachBottom(){
    wx.showToast({
      title: '暂无更多数据',
      duration:1000
    })
  }

})
