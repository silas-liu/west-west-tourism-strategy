const apiURL = 'http://www.bomp.com.cn/api'

const wxRequest = (params,url) => {
  wx.request({
    url: 'url',
    method:params.method || 'GET',
    data:params.data || {},
    header:{
      Accept:'application/json',
      'Content-Type':'application/json'
    },
    success(res) {
      if(params.success) {
        params.success(res)
      }
    },
    fail(res){
      if(params.fail) {
        params.fail(res)
      }
    },
    complete(res) {
      if(params.complete) {
        params.complete(res)
      }
    }
  })
} 

const getHotTripList = (params) => {
  wxRequest(params,`${apiURL}/v2/index/`)
}

const getExplorePlaceList = (params) => {
  wxRequest(params,`${apiURL}/destination/v3/`)
}

const getPlaceInfoById = (params) => {
  wxRequest(params,`${apiURL}/destination/place/${params.query.type}/${params.query.id}/`)
}

const getPlacePOIById = (params) => {
  wxRequest(params,`${apiURL}/destination/place/${params.query.type}/${params.query.id}/pois/${params.query.poiType}/`)
}

const getTripInfoById = (params) => {
  wxRequest(params,`${apiURL}/trips/${params.query.tripId}/waypoints/`)
}

const getPlaceTripById = (params) => {
  wxRequest(params,`${apiURL}/destination/place/${params.query.type}/${params.query.id}/trips/`)
}

const getUserInfoById = (params) => {
  wxRequest(params,`${apiURL}/users/${params.query.userId}/v2`)
}

const getWaypointInfoById = (params) => {
  wxRequest(params,`${apiURL}/trips/${params.query.tripId}/waypoints/${params.query.waypointId}`)
}

const getWaypointReplyById = (params) => {
  wxRequest(params,`${apiURL}/trips/${params.query.tripId}/waypoints/${params.query.waypointId}/replies/`)
}

module.exports = {
    getHotTripList,
  getExplorePlaceList,
  getPlaceInfoById,
  getPlacePOIById,
  getTripInfoById,
  getPlaceTripById,
  getUserInfoById,
  getWaypointInfoById,
  getWaypointReplyById,
}